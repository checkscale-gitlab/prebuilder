Scripts for building an image containing [`prebuilder.py`](https://gitlab.com/KOLANICH/prebuilder.py)
=====================================================================================================

Scripts in this repo are [Unlicensed ![](https://raw.githubusercontent.com/unlicense/unlicense.org/master/static/favicon.png)](https://unlicense.org/).

Third-party components have own licenses.

**DISCLAIMER: BADGES BELOW DO NOT REFLECT THE STATE OF THE DEPENDENCIES IN THE CONTAINER**

This builds a Docker image contains the following components:
* [`registry.gitlab.com/kolanich-subgroups/docker-images/prebuilder_dependencies:latest` Docker image](https://gitlab.com/KOLANICH-subgroups/docker-images/prebuilder_dependencies), each component of which has own license.

* [`prebuilder.py`](https://gitlab.com/KOLANICH/prebuilder.py)[![PyPi Status](https://img.shields.io/pypi/v/prebuilder.svg)](https://pypi.org/pypi/prebuilder)[![Libraries.io Status](https://img.shields.io/librariesio/github/KOLANICH/prebuilder.py.svg)](https://libraries.io/github/KOLANICH/prebuilder.py)![Licence](https://img.shields.io/github/license/KOLANICH/prebuilder.py.svg)
